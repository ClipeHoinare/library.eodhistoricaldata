﻿using Library.Repository.StorageArmory;
using Library.ThinkOrSwim.DataService.Processor;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library.EODHistoricalData.DataService.Tests.Processor
{
    [TestClass]
    public class EodHistMarketOptionsDataProcessorTests
    {
        [TestMethod]
        public void TosStockGetData()
        {
            (new StorageSqlQueue()).Start();
            var c1 = new EodHistMarketOptionsDataProcessor();

            c1.Process(null);

            (new StorageSqlQueue()).Stop();
        }
    }
}
