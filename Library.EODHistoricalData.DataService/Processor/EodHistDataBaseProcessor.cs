﻿using EODHistoricalData.NET;
using Library.Domain.Infrastructure;
using Library.Domain.Jackdow;
using Library.Domain.Trade.Base;
using Library.Repository.Base;
using Library.Repository.LogRepository;
using Library.Repository.StorageArmory;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Library.EODHistoricalData.DataService.Processor
{
    public class EodHistDataBaseProcessor
    {
    }
    public abstract class TosDataBaseProcessor<T>
    {
        #region variables
        protected IImporterSync<Options, FinanceParameter> _financeImporter = null;

        protected ClassFactory _optionMetricsFactory = new ClassFactory(ClassFactory.DenKey.OptionMetrics);

        protected bool _sqlDebug = true;
        protected string _lastSqlDebugString = string.Empty;
        protected DateTime _initialDateTime = DateTime.Parse("1960-1-1");
        protected string _crlf = "\n\r";

        public TosDataBaseProcessor()
        {
        }
        #endregion
        #region public area
        public bool Process(string stringParameters = null)
        {
            bool succesfully = true;

            DateTime fromDate;
            DateTime toDate;
            fromDate = DateTime.Parse("2018-01-01").AddDays(-1);
            toDate = DateTime.Parse("2020-12-31");

            while (fromDate < toDate)
            {
                fromDate = fromDate.AddDays(1);

                    DoImporting(fromDate);                
            }

            return succesfully;
        }
        #endregion
        #region protected area
        protected void DoImporting(DateTime tradingDate)
        {
            //Get a list of stocks with their flags
            var stockList = GetStockIdsForPull();

            //loop thru the stock list and process one by one
            for (int cntStock = 0; cntStock < stockList.Count; cntStock++)
            {
                var symbol = Convert.ToString(stockList[cntStock]["ticker"]);
                var secid = Convert.ToInt32(stockList[cntStock]["secid"]);

                GetIndividualStockValues(secid, symbol, tradingDate);
            }

            AfterProcessing(null);
        }
        protected void GetIndividualStockValues(int secId, string symbol, DateTime tradingDate)
        {
            bool found = false;
            var secType = "STK";

            var parameter = new FinanceParameter() { Symbol = symbol, SecType=secType, StartDateTime=tradingDate, EndDateTime=tradingDate.AddDays(1) };
            var tuple = _financeImporter.ImportSync(parameter);
            if (tuple != null && tuple.Item2 is null && tuple.Item1 != null && tuple.Item1.Count > 0)
            {
                BeforeImporting(tuple.Item1, secId);
                var days = GetDaysForSAS(tradingDate);
                var seconds = 16*3600 + 0*60 + 0;
                SaveStockValuesBySQL(tuple.Item1, secId, days, seconds);
                found = true;
            }
            else
            {
                if (tuple.Item2 != null)
                    ErrorLogger.LogException(tuple.Item2, ErrorLevel.Error, ErrorLoggerName.YahooBox, String.Format("TosFinanceBaseProcessor.GetIndividualStockValues, stockID={0}", secId));
            }

            AfterImporting(tuple.Item1, (found ? 1 : 0), secId);
        }
        protected void SaveStockValuesBySQL(dynamic items, int stockID, int tradingDate, int tradingTime)
        {
            if (items != null && items.Count > 0)
            {
                    var options = (Options)items[0];

                if (options.Data != null && options.Data.Count > 0)
                {
                    for (int cnt = 0; cnt < options.Data.Count; cnt++)
                    {
                        var datum = options.Data[cnt];
                        if (datum.Options != null && datum.Options.Call != null && datum.Options.Call.Count > 0)
                        {
                            for (int cpn = 0; cpn < datum.Options.Call.Count; cpn++)
                            {
                                var cp = datum.Options.Call[cpn];

                                SaveOneLittleThing(stockID, tradingDate, tradingTime, cp);
                            }
                        }
                        if (datum.Options != null && datum.Options.Put != null && datum.Options.Put.Count > 0)
                        {
                            for (int cpn = 0; cpn < datum.Options.Put.Count; cpn++)
                            {
                                var cp = datum.Options.Put[cpn];

                                SaveOneLittleThing(stockID, tradingDate, tradingTime, cp);
                            }
                        }
                    }
                }
            }
        }

        protected void SaveOneLittleThing(int stockID, int tradingDate, int tradingTime, Characteristics cp)
        {
            var data = GetCommandAndParameterForSave(new Tuple<int, int, int>(stockID, tradingDate, tradingTime), cp);

            SetLastSqlString(data.Item1, data.Item2);

            StorageSqlQueue.RingQueue.AddItem((new StorageSqlQueue.SqlBundle(ClassFactory.DenKey.OptionMetrics) { CommandText = data.Item1, Parameters = data.Item2 }));
        }
        protected int GetDaysForSAS(DateTime? dt)
        {
            var days = 0;
            if (dt.HasValue)
                days = (dt.Value.Date - _initialDateTime).Days;
            return days;
        }
        protected int GetSecsForSAS(DateTime? dt)
        {
            var secs = 0;
            if (dt.HasValue)
                secs = dt.Value.Date.Hour * 3600 + dt.Value.Date.Minute * 60 + dt.Value.Date.Second ;
            return secs;
        }
        protected void SetLastSqlString(string sqlCommand, List<SqlParameter> parameters = null, CommandType commandType = CommandType.Text)
        {
            if (_sqlDebug)
            {
                _lastSqlDebugString = SqlGenerator.CreateExecutableSqlStatement(sqlCommand, parameters);
            }
        }
        #endregion
        #region protected abstract/virtual area
        protected abstract Tuple<string, List<SqlParameter>> GetCommandAndParameterForSave(Tuple<int, int, int> stockID, dynamic item);
        protected abstract List<Dictionary<string, object>> GetStockIdsForPull();
        protected abstract DateTime GetNextDateToProcess(DateTime fromDate);
        protected virtual void BeforeImporting(dynamic items, int stockID)
        {
        }
        protected virtual void AfterImporting(dynamic items, dynamic flags, int stockID)
        {
        }
        protected virtual void AfterProcessing(dynamic flags)
        {
        }
        #endregion
    }
}
