﻿using EODHistoricalData.NET;
using Library.Domain.Jackdow;
using Library.EODHistoricalData.DataService.Importer;
using Library.EODHistoricalData.DataService.Processor;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Library.ThinkOrSwim.DataService.Processor
{
    public class EodHistMarketOptionsDataProcessor : TosDataBaseProcessor<Options>
    {
        public EodHistMarketOptionsDataProcessor()
        {
            _financeImporter = new EodHistMarketImporter();
        }
        protected override List<Dictionary<string, object>> GetStockIdsForPull()
        {
            string sqlCommand = @"
                select distinct sec.secid, sec.ticker, sec.index_flag
	                from OptionMetrics.dbo.[hlpSECURD] hlp 
	                inner join [OptionMetricsRaw].[dbo].[SECURD] sec on sec.secid=hlp.secid
					where hlp.ticker='AAPL'
 " + _crlf;

            SetLastSqlString(sqlCommand);

            var ds = _optionMetricsFactory.GetDataSet(sqlCommand);

            return _optionMetricsFactory.ConvertToList(ds);
        }
        protected override DateTime GetNextDateToProcess(DateTime dt)
        {
            return dt.AddDays(1);
        }
        protected override Tuple<string, List<SqlParameter>> GetCommandAndParameterForSave(Tuple<int, int, int> tranInfo, dynamic input)
        {
            string sqlCommand = @" 
                if not exists (select * from OPPRCD where secid=@secid and date=@LastTradeDate and time=@LastTradeTime and source=@source and Symbol=@Symbol and symbol_flag=@symbol_flag)
                begin
	                INSERT INTO [OPPRCD] (secid, date, time, source, Symbol, symbol_flag
                        , best_offer, best_bid, [Delta], exdate, [forward_price], Gamma
                        , impl_volatility, LastPrice, last_date, cp_flag, [open_interest], [strike_price], Theta, [Vega], [Volume]
                        , cfadj, [am_settlement], [contract_size], [ss_flag]) 
                    values (@secid, @LastTradeDate, @LastTradeTime, @source, @Symbol, @symbol_flag
                        , @best_offer ,@best_bid , @Delta, @exdate, @Theoretical, @Gamma
                        , @impl_volatility, @LastPrice, @last_date, @cp_flag, @open_interest, @strike_price, @Theta , @Vega, @Volume
                        , 1, 0, 100, 3);
                end
                else
                if 1=0
                begin
	                UPDATE OPPRCD set best_offer=@best_offer, best_bid=@best_bid, [Delta]=@Delta, exdate=@exdate, [forward_price]=@Theoretical, Gamma=@Gamma
                        , impl_volatility=@impl_volatility, LastPrice=@LastPrice, last_date=@last_date, cp_flag=@cp_flag
                        , @open_interest=[open_interest], strike_price=@strike_price, Theta=@Theta, Vega=@Vega, Volume=@Volume
                        , ss_flag=4
                        where secid=@secid and date=@LastTradeDate and time=@LastTradeTime and source=@source and Symbol=@Symbol and symbol_flag=@symbol_flag;
                end
";

            List<SqlParameter> parameters = new List<SqlParameter>();

            var item = input as Characteristics;

            parameters.Add(new SqlParameter("secid", tranInfo.Item1));
            parameters.Add(new SqlParameter("Date", tranInfo.Item2));
            parameters.Add(new SqlParameter("Time", tranInfo.Item3));
            parameters.Add(new SqlParameter("Source", 2));
            parameters.Add(new SqlParameter("Symbol", item.ContractName));
            parameters.Add(new SqlParameter("symbol_flag", 1));

            parameters.Add(new SqlParameter("best_offer", item.Ask));
            parameters.Add(new SqlParameter("best_bid", item.Bid));
            parameters.Add(new SqlParameter("Delta", item.Delta));
            parameters.Add(new SqlParameter("exdate", GetDaysForSAS(item.ExpirationDate.UtcDateTime)));
            parameters.Add(new SqlParameter("Gamma", item.Gamma));
            parameters.Add(new SqlParameter("impl_volatility", item.ImpliedVolatility));
            parameters.Add(new SqlParameter("LastPrice", item.LastPrice));
            parameters.Add(new SqlParameter("LastTradeDate", GetDaysForSAS(item.LastTradeDateTime)));
            parameters.Add(new SqlParameter("LastTradeTime", GetSecsForSAS(item.LastTradeDateTime)));
            parameters.Add(new SqlParameter("cp_flag", item.OptionType.ToString().Substring(0,1)));
            parameters.Add(new SqlParameter("open_interest", item.OpenInterest));
            parameters.Add(new SqlParameter("Rho", item.Rho));
            parameters.Add(new SqlParameter("strike_price", item.Strike));
            parameters.Add(new SqlParameter("Theoretical", item.Theoretical));
            parameters.Add(new SqlParameter("Theta", item.Theta));
            parameters.Add(new SqlParameter("last_date", GetDaysForSAS(item.UpdatedAt.UtcDateTime)));
            parameters.Add(new SqlParameter("Vega", item.Vega));
            parameters.Add(new SqlParameter("Volume", item.Volume));

            return new Tuple<string, List<SqlParameter>>(sqlCommand, parameters);
        }

        //private System.Data.SqlDbType GetDaysForSAS(DateTime? lastTradeDateTime)
        //{
        //    throw new NotImplementedException();
        //}

        //private System.Data.SqlDbType GetDaysForSAS(DateTimeOffset expirationDate)
        //{
        //    throw new NotImplementedException();
        //}

        protected override void BeforeImporting(dynamic input, int stockID)
        {
            var item = input as QuoteRealTimeItem;

           // item.TradingDate = 



        }

        protected override void AfterImporting(dynamic items, dynamic flags, int stockID)
        {
            var flag = flags as int?;
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("stockID", stockID));
            parameters.Add(new SqlParameter("enabledFlag", flag));

            string sqlCommand =
                " update st " +
                " set st.[Enabled] = @enabledFlag " +
                " from stock st " +
                " where st.StockID = @stockID ";

            SetLastSqlString(sqlCommand, parameters);
        }
    }
}
