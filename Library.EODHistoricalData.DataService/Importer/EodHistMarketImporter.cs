﻿using EODHistoricalData.NET;
using Library.Domain.Infrastructure;
using Library.Domain.Jackdow;
using Library.Domain.Trade.Base;
using System;
using System.Collections.Generic;

namespace Library.EODHistoricalData.DataService.Importer
{
    public class EodHistMarketImporter : IImporterSync<Options, FinanceParameter>
    {
        private static readonly DateTime UnixMinDateTime = new DateTime(1901, 12, 13);
        private static readonly DateTime UnixMaxDateTime = new DateTime(2038, 1, 19);

        private EODHistoricalDataClient _client;

        public EODHistoricalDataClient Client { get => _client; set => _client = value; }

        public EodHistMarketImporter() : this(new EODHistoricalDataClient("OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX", true))
        {

        }

        public EodHistMarketImporter(EODHistoricalDataClient controller)
        {
            _client = controller;
        }

        /// <summary>
        /// TODO: Topics should be mapped to a Trady common domain
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="endDateTime"></param>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public Tuple<IReadOnlyList<Options>, Exception> ImportSync(FinanceParameter parameter)
        {
            var quotes = new List<Options>();
            Exception retEx = null;
            try
            {

                var corrStartTime = parameter.StartDateTime < UnixMinDateTime ? UnixMinDateTime : parameter.StartDateTime;
                var corrEndTime = (parameter.EndDateTime > UnixMaxDateTime ? UnixMaxDateTime : parameter.EndDateTime) ?? UnixMaxDateTime;

                quotes.Add(_client.GetOptions(parameter.Symbol, corrStartTime, null, corrStartTime, corrEndTime));
            }
            catch (Exception ex)
            {
                quotes = null;
                retEx = ex;
            }

            return new Tuple<IReadOnlyList<Options>, Exception>(quotes, retEx);
        }
    }
}
